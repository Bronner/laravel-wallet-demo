# Демо проект

## Как развернуть

1. Скопировать и настроить файл с переменными окружения для работы с БД mysql
    ```
    cp .env.example .env
    ```

2. Установить зависимости
    ```
    composer install
    ```
3. Сгенерировать ключ
    ```
    php artisan key:generate
    ```
4. Выполнить миграции БД
    ```
    php artisan migrate --seed
    ```


## Доступные запросы

### Информация по кошельку
```
curl --request GET \
  --url http://localhost/api/wallet/1 \
  --header 'content-type: application/json' \
  --header 'x-requested-with: XMLHttpRequest'
```
Ответ 200:
```
{
  "id": 1,
  "balance": 275.16,
  "currency_code": "USD",
  "created_at": "2019-12-06 00:00:00",
  "updated_at": "2019-12-06 00:00:00"
}
```

### Изменение баланса
```
curl --request POST \
  --url http://localhost/api/wallet/1/transaction \
  --header 'content-type: application/json' \
  --header 'x-requested-with: XMLHttpRequest' \
  --data '{
	"amount": 1234,
	"type": "debit",
	"reason": "refund",
	"currency_code": "RUB"
}'
```

Ответ 201:
```
{
  "amount": 1234,
  "type": "debit",
  "reason": "refund",
  "currency_code": "RUB",
  "converted_amount": 19.2504,
  "wallet_id": 1,
  "updated_at": "2019-12-06 00:00:00",
  "created_at": "2019-12-06 00:00:00",
  "id": 26
}
```

### Сумма возвратов в валюте кошелька за последние 7 дней

```
curl --request GET \
  --url http://localhost/api/wallet/1/history/refund \
  --header 'content-type: application/json' \
  --header 'x-requested-with: XMLHttpRequest'

```
Ответ 200:
```
{
  "amount": "1069.4668",
  "currency": "USD"
}
```

## Ответы на вопросы

- Написать SQL запрос, который вернет сумму, полученную по причине refund за последние 7 дней.

```sql
select sum(converted_amount) as total from `wallet_transactions` where `reason` = 'refund' and `wallet_id` = 1 and `created_at` >= '2019-11-29';
```
