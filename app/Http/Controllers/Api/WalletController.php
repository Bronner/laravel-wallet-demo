<?php

namespace App\Http\Controllers\Api;

use App\Domain\Wallet\Models\Wallet;
use App\Http\Controllers\Controller;

class WalletController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  Wallet  $wallet
     *
     * @return Wallet
     */
    public function show(Wallet $wallet)
    {
        return $wallet;
    }
}
