<?php

namespace App\Http\Controllers\Api;

use App\Domain\Wallet\CurrencyConverter;
use App\Domain\Wallet\Models\Currency;
use App\Domain\Wallet\Models\Wallet;
use App\Domain\Wallet\Models\WalletTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class WalletTransactionController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @param  Wallet  $wallet
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws ValidationException
     * @throws \App\Domain\Wallet\UnsupportedCurrency
     */
    public function store(Request $request, Wallet $wallet)
    {
        $this->validate(
            $request,
            [
                'amount' => 'required|numeric|gt:0',
                'type' => [
                    'required',
                    Rule::in([WalletTransaction::TYPE_CREDIT, WalletTransaction::TYPE_DEBIT]),
                ],
                'reason' => [
                    'required',
                    Rule::in([WalletTransaction::REASON_STOCK, WalletTransaction::REASON_REFUND]),
                ],
                'currency_code' => [
                    'required',
                    Rule::in([Currency::RUB, Currency::USD]),
                ],
            ]
        );

        $payload = $request->all(['amount', 'type', 'reason', 'currency_code']);

        $converter = new CurrencyConverter();
        $convertedAmount = $converter->convert($payload['amount'], $payload['currency_code'], $wallet->currency_code);
        $payload['converted_amount'] = $convertedAmount;

        $transaction = $wallet->transactions()->create($payload);
        $wallet->recalculate();

        return $transaction;
    }
}
