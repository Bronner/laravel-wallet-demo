<?php

namespace App\Http\Controllers\Api;

use App\Domain\Wallet\Models\Wallet;
use App\Domain\Wallet\Models\WalletTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;

class WalletHistoryController extends Controller
{
    /**
     * История возвратов за последние 7 дней
     *
     * @param  Wallet  $wallet
     *
     * @return JsonResponse
     */
    public function refundHistory(Wallet $wallet): JsonResponse
    {
        $historyFrom = Carbon::now()->subDays(7);
        $amount = WalletTransaction::ofReason(WalletTransaction::REASON_REFUND)->where('wallet_id', $wallet->id)->where(
                'created_at',
                '>=',
                $historyFrom
            )->sum('converted_amount');

        return response()->json(
            [
                'amount' => $amount,
                'currency' => $wallet->currency_code,
            ]
        );
    }
}
