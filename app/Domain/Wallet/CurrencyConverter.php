<?php

namespace App\Domain\Wallet;

use App\Domain\Wallet\Models\CurrencyRate;

class CurrencyConverter
{
    /**
     * @param  float  $amount
     * @param  string  $from
     * @param  string  $to
     *
     * @return float
     * @throws UnsupportedCurrency
     */
    public function convert(float $amount, string $from, string $to): float
    {
        if ($from === $to) {
            return $amount;
        }

        $model = CurrencyRate::where('base_currency', $from)->where('currency', $to)->first(['rate']);

        if (!$model) {
            throw new UnsupportedCurrency(sprintf('Unable to convert %s to %s', $from, $to));
        }

        return $amount * $model['rate'];
    }
}
