<?php

namespace App\Domain\Wallet\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyRate extends Model
{
    protected $casts = [
        'rate' => 'float'
    ];
}
