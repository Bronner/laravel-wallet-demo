<?php

namespace App\Domain\Wallet\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public const USD = 'USD';

    public const RUB = 'RUB';
}
