<?php

namespace App\Domain\Wallet\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;

/**
 * Class Wallet
 *
 * @property mixed currency_code
 * @property Collection transactions
 * @package App\Domain\Wallet\Models
 */
class Wallet extends Model
{
    protected $guarded = [];

    public function getBalanceAttribute(float $value): float
    {
        return round($value, 2);
    }

    public function transactions(): HasMany
    {
        return $this->hasMany(WalletTransaction::class);
    }

    public function currency(): HasOne
    {
        return $this->hasOne(Currency::class);
    }

    public function recalculate()
    {
        $amount = $this->transactions->sum(
            function (WalletTransaction $item) {
                if ($item->type === WalletTransaction::TYPE_DEBIT) {
                    return -$item->converted_amount;
                }

                return $item->converted_amount;
            }
        );

        $this->update(
            [
                'balance' => $amount,
            ]
        );
    }
}
