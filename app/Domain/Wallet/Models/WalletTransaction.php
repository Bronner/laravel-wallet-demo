<?php

namespace App\Domain\Wallet\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class WalletTransaction extends Model
{
    public const TYPE_CREDIT = 'credit';

    public const TYPE_DEBIT = 'debit';

    public const REASON_STOCK = 'stock';

    public const REASON_REFUND = 'refund';

    protected $guarded = [];

    final public function wallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class);
    }

    final public function currency(): HasOne
    {
        return $this->hasOne(Currency::class);
    }

    /**
     * Возвращает транзакции по причине
     *
     * @param  Builder  $query
     * @param  string  $reason
     *
     * @return Builder
     */
    final public function scopeOfReason(Builder $query, string $reason): Builder
    {
        return $query->where('reason', $reason);
    }
}
