<?php

use App\Domain\Core\Models\User;
use App\Domain\Wallet\Models\Wallet;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddWalletIdToUsersTable extends Migration
{
    /**
     * @var string
     */
    private $table;

    /**
     * @var string
     */
    private $walletsTable;

    public function __construct()
    {
        $this->table = (new User())->getTable();
        $this->walletsTable = (new Wallet())->getTable();
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            $this->table,
            function (Blueprint $table) {
                $table->unsignedBigInteger('wallet_id');
                $table->foreign('wallet_id')->references('id')->on($this->walletsTable)->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            $this->table,
            function (Blueprint $table) {
                $table->dropForeign('wallet_id');
                $table->dropColumn('wallet_id');
            }
        );
    }
}
