<?php

use App\Domain\Wallet\Models\Currency;
use App\Domain\Wallet\Models\CurrencyRate;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCurrencyRatesTable extends Migration
{
    /**
     * @var string
     */
    private $table;

    /**
     * @var string
     */
    private $currencyTable;

    public function __construct()
    {
        $this->table = (new CurrencyRate())->getTable();
        $this->currencyTable = (new Currency())->getTable();
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            $this->table,
            function (Blueprint $table) {
                $table->increments('id');
                $table->char('base_currency', 3);
                $table->char('currency', 3);
                $table->unsignedDecimal('rate', 18, 4)->default(0);
                $table->timestamps();

                $table->unique(['base_currency', 'currency']);
                $table->foreign('base_currency')->on($this->currencyTable)->references('code')->onDelete('cascade');
                $table->foreign('currency')->on($this->currencyTable)->references('code')->onDelete('cascade');
            }
        );

        DB::table($this->table)->insert(
            [
                [
                    'base_currency' => Currency::USD,
                    'currency' => Currency::RUB,
                    'rate' => 64,
                ],
                [
                    'base_currency' => Currency::RUB,
                    'currency' => Currency::USD,
                    'rate' => 1/64
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
