<?php

use App\Domain\Wallet\Models\Currency;
use App\Domain\Wallet\Models\Wallet;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWalletsTable extends Migration
{
    /**
     * @var string
     */
    private $table;

    /**
     * @var string
     */
    private $currencyTable;

    public function __construct()
    {
        $this->table = (new Wallet())->getTable();
        $this->currencyTable = (new Currency())->getTable();
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            $this->table,
            function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->decimal('balance', 18, 4)->default(0);

                $table->char('currency_code', 3);
                $table->foreign('currency_code')->on($this->currencyTable)->references('code')->onDelete('cascade');

                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
