<?php

use App\Domain\Wallet\Models\Currency;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCurrenciesTable extends Migration
{
    /**
     * @var string
     */
    private $table;

    public function __construct()
    {
        $this->table = (new Currency())->getTable();
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            $this->table,
            function (Blueprint $table) {
                $table->char('code', 3)->primary();
                $table->timestamps();
            }
        );

        DB::table($this->table)->insert(
            [
                [
                    'code' => Currency::USD,
                ],
                [
                    'code' => Currency::RUB,
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
