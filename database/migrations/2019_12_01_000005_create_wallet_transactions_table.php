<?php

use App\Domain\Wallet\Models\Currency;
use App\Domain\Wallet\Models\Wallet;
use App\Domain\Wallet\Models\WalletTransaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWalletTransactionsTable extends Migration
{
    /**
     * @var string
     */
    private $table;

    /**
     * @var string
     */
    private $walletTable;

    /**
     * @var string
     */
    private $currencyTable;

    public function __construct()
    {
        $this->table = (new WalletTransaction())->getTable();
        $this->walletTable = (new Wallet())->getTable();
        $this->currencyTable = (new Currency())->getTable();
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            $this->table,
            function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->decimal('amount', 18, 4);
                $table->decimal('converted_amount', 18, 4);

                $table->enum('type', ['debit', 'credit'])->index();

                $table->enum('reason', ['stock', 'refund'])->index();

                $table->unsignedBigInteger('wallet_id');
                $table->foreign('wallet_id')->on($this->walletTable)->references('id')->onDelete('cascade');

                $table->char('currency_code', 3);
                $table->foreign('currency_code')->on($this->currencyTable)->references('code')->onDelete('cascade');

                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
