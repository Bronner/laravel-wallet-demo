<?php

use App\Domain\Wallet\Models\Currency;
use App\Domain\Wallet\Models\Wallet;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */

$factory->define(
    Wallet::class,
    function (Faker $faker) {
        return [
            'balance' => $faker->numberBetween(10000),
            'currency_code' => $faker->randomElement([Currency::RUB, Currency::USD]),
        ];
    }
);
