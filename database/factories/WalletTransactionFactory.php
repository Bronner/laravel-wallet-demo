<?php

use App\Domain\Wallet\Models\Currency;
use App\Domain\Wallet\Models\Wallet;
use App\Domain\Wallet\Models\WalletTransaction;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */

$factory->define(
    WalletTransaction::class,
    function (Faker $faker) {
        return [
            'amount' => $faker->numberBetween(1, 10000),
            'converted_amount' => $faker->numberBetween(1, 10000),
            'type' => $faker->randomElement([WalletTransaction::TYPE_CREDIT, WalletTransaction::TYPE_DEBIT]),
            'reason' => $faker->randomElement([WalletTransaction::REASON_REFUND, WalletTransaction::REASON_STOCK]),
            'wallet_id' => factory(Wallet::class),
            'currency_code' => $faker->randomElement([Currency::RUB, Currency::USD]),
        ];
    }
);
