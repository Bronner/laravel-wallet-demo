<?php

use App\Domain\Core\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // System user
        factory(User::class)->create(
            [
                'email' => 'admin@localhost',
                'password' => bcrypt('password'),
            ]
        );
    }
}
