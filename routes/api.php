<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(
    function () {
        Route::apiResource('wallet', 'WalletController')->only(['show']);
        Route::apiResource('wallet.transaction', 'WalletTransactionController')->only(['store']);
        Route::get('wallet/{wallet}/history/refund', 'WalletHistoryController@refundHistory')->name('wallet.history.refund');
    }
);
